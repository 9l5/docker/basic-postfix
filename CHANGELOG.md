# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.2.11](../../compare/1.2.10...1.2.11) (2024-12-01)

### [1.2.10](../../compare/1.2.9...1.2.10) (2024-11-01)

### [1.2.9](../../compare/1.2.8...1.2.9) (2024-10-01)

### [1.2.8](../../compare/1.2.7...1.2.8) (2024-09-01)

### [1.2.7](../../compare/1.2.6...1.2.7) (2024-08-01)

### [1.2.6](../../compare/1.2.5...1.2.6) (2024-07-01)

### [1.2.5](../../compare/1.2.4...1.2.5) (2024-06-01)

### [1.2.4](../../compare/1.2.3...1.2.4) (2024-05-01)

### [1.2.3](../../compare/1.2.2...1.2.3) (2024-04-01)

### [1.2.2](../../compare/1.2.1...1.2.2) (2024-03-01)

### [1.2.1](../../compare/1.2.0...1.2.1) (2024-02-01)

## [1.2.0](../../compare/1.1.1...1.2.0) (2024-01-22)


### Features

* add postfix-pcre to install list ([b9fccb7](b9fccb715ade8bac3f8b59b7748afd3964f7bf39))

### [1.1.1](../../compare/1.1.0...1.1.1) (2024-01-01)

## [1.1.0](../../compare/1.0.9...1.1.0) (2023-12-28)


### Features

* update to ubuntu 24.04 ([2cd2fcd](2cd2fcdc377727d868f90b22ea86055901e87e4e))

### [1.0.9](../../compare/1.0.8...1.0.9) (2023-11-01)

### [1.0.8](../../compare/1.0.7...1.0.8) (2023-10-01)

### [1.0.7](../../compare/1.0.6...1.0.7) (2023-09-01)

### [1.0.6](../../compare/1.0.5...1.0.6) (2023-08-01)

### [1.0.5](../../compare/1.0.4...1.0.5) (2023-07-01)

### [1.0.4](../../compare/1.0.3...1.0.4) (2023-06-01)

### [1.0.3](../../compare/1.0.2...1.0.3) (2023-04-01)

### [1.0.2](../../compare/1.0.1...1.0.2) (2023-02-01)

### [1.0.1](../../compare/1.0.0...1.0.1) (2023-01-01)

## 1.0.0 (2022-11-29)


### Features

* add libsasl2-modules for authentication ([de8b382](de8b38235bb9ce0f0a5c70b66a0a00686eee1da2))
* ugrade repo to new auto-updateable state ([fdbcc7d](fdbcc7d31eba48d682cfcf57461bdd3a2fdfda49))
* update to ubuntu 22.04 ([7efecab](7efecabce1fd7bdd7a36fb2d6e3b56bb00cfdda8))
